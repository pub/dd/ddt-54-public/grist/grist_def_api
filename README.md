# Fonctions exploitant l'API de Grist avec un proxy

Ce script comporte 4 fonctions.

## Fonction "param_api_grist"

Cette fonction permet de stocker dans un dictionnaire les paramètres généraux utilisés par l'API. Cela permet d'éviter de les renseigner à chaque appel ultérieur de fonction.
Cette fonction nécessite 4 paramètres : 
- *server* : l'URL de l'application GRIST (exemple : https://grist.incubateur.anct.gouv.fr)
- *api_key* : la clé de l'API propre à l'utilisateur. Elle est indiquée dans les paramètres du compte
- *id_doc* : l'id du document pour l'API, indiquée dans la page "Paramètres"
- *proxies* : le proxy, sous forme de dictionnaire

La fonction retourne un dictionnaire contenant ces 4 paramètres.

Exemple : 
```
#défintion des variables
http_proxy = "http://xxxxxxxxx:numport"
https_proxy = "https://xxxxxxxxx:numport"
proxies = { "http":http_proxy, "https":https_proxy  }

server = "https://grist.incubateur.anct.gouv.fr"
id_doc = "XXXXXXXXXXXXXXXXXXXX"
api_key = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
id_table = "Table_name"

#Appel de la fonction
param_api = param_api_grist(server, api_key, id_doc, proxies)
```

## Fonction "fetch_table"

Cette fonction permet de récupérer toutes les données d'une table de GRIST et de les stoker dans un dataframe.
Cette fonction nécessite 2 paramètres :
- *param_api* : un dictionnaire avec les paramètres, créé par la fonction 'param_api_grist'
- *id_table* : l'id de la table telle qu'indiquée à la page "Données sources"

La fonction retourne un dataframe contentant toutes les données de la table.

Exemple :
```
df = fetch_table(param_api, id_table)

```

## Fonction "update_table"

Cette fonction permet de modifier ou d'ajouter des données en s'appuyant sur une colonne de jointure :
- si la valeur de jointure existe dans la table cible : 
  - si la donnée existe -> modifie la donnée
  - si la donnée n'existe pas -> ajoute la donnée
- si la valeur de jointure n'existe pas dans la table cible :
  - ajoute une ligne dans la table cible

En cas de doublon sur la valeur de jointure, seule la première rencontrée est affectée.


Cette fonction nécessite 4 paramètres :
- *param_api* : un dictionnaire avec les paramètres, créé par la fonction 'param_api_grit'
- *id_table* : l'id de la table telle qu'indiquée à la page "Données sources"
- *data_update* : un dataframe contenant les données à ajouter/modifier et la donnée de jointure
- *col_require* : le nom de la colonne de jointure dans le dataframe 'data_update'

La fonction retourne le code d'exécution de la requète (valeur 200 si tout c'est bien passé)

Exemple :
```
#colonne de jointure pour l'ajout/modification :
col_require = "ID"

#dataframe à ajouter/modifier
df_add = pd.DataFrame({"ID" : [43404965600012, 43422794800016], 
                       "Date" : ["17/07/2024", "17/07/2024"],
                       "Realized": ["True", "True"]})

#Appel de la fonction
update_table(param_api, id_table, df_add, col_require)                      

```


## Fonction "delete_row"

Cette fonction supprime des lignes d'une table en utilisant les numéros de ligne.
Cette fonction nécessite 3 paramètres :
- *param_api* : un dictionnaire avec les paramètres, créé par la fonction 'param_api_grit'
- *id_table* : l'id de la table telle qu'indiquée à la page "Données sources"
- *data_delete* : une liste contenant les numéros des lignes à supprimer

La fonction retourne le code d'exécution de la requète (valeur 200 si tout c'est bien passé)

Exemple :
```
#Définition d'une liste pour supprimer les lignes 1, 2 et 3 de la table dans Grist :
data_delete = [1,2,3]

#Appel de la fonction
delete_row(param_api, id_table, data_delete)                      

```





