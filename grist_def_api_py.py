import pandas as pd
import io
import requests
import json

# fonction pour stocker les paramètres généraux de l'api et éviter de les renseigner tous à chaque fois
def param_api_grist(server:str, api_key:str, id_doc:str, proxies:dict):
    '''
    Stocke dans un dictionnaire les paramètres généraux pour l'appel de l'api
    :server : l'URL de l'application GRIST (exemple : https://grist.incubateur.anct.gouv.fr)
    :api_key : clé de l'API propre à l'utilisateur, indiqué dans les paramètres du compte
    :id_doc : l'id du document pour l'API, indiquée dans la page "Paramètres"
    :proxies : le proxy, sous forme de dictionnaire
    '''
    return {"server" : server,  "api_key" : api_key, "id_doc" : id_doc, "proxies":proxies}

# fonction qui récupère toutes les données d'une table et les stocke dans un dataframe
def fetch_table (param_api:dict, id_table:str):
    '''
    Récupère toutes les données d'une table par son nom, et les stocke dans un dataframe
    :param_api : un dictionnaire avec les paramètres, créé par la fonction 'param_api_grist'
    :id_table : l'id de la table telle qu'indiquée à la page "Données sources"
    '''
    #contruction de l'url pour récupérer les données sous forme de csv
    url_csv = "%s/api/docs/%s/download/csv?tableId=%s&header=label" % (param_api["server"], param_api["id_doc"], id_table)
    
    #construction du header de l'appel de l'API, avec la clé utilisateur 'api_key' et type csv
    headers={
        'Authorization': 'Bearer %s' % param_api["api_key"],
        'Content-Type': 'application/csv',
        'Accept': 'application/csv',
      }
    
    #récupération des données sous forme de csv 
    csv_file = requests.get(url_csv, proxies=param_api["proxies"], headers=headers).text
    
    #retourne les données sous forme de DataFrame
    df = pd.read_csv(io.StringIO(csv_file))
    #ajoute le numéro de la ligne, référence à utiliser en cas de suppression de ligne
    df["no_ligne"] = df.index + 1
    return(df)

# fonction qui permet de modifier ou d'ajouter des données en s'appuyant sur une colonne de jointure :
#  - si la valeur de jointure existe dans la table cible : 
#        - si la donnée existe -> modifie la donnée
#        - si la donnée n'existe pas -> ajoute la donnée
#  - si la valeur de jointure n'existe pas dans la table cible :
#       -  ajoute une ligne dans la table cible
# En cas de doublon sur la valeur de jointure, seule la première rencontrée est affectée
def update_table(param_api:dict, id_table:str, data_update:pd.DataFrame, col_require:str):
    ''' 
    Ajoute ou modifie si existe des données d'une table par son nom
    :param_api : un dictionnaire avec les paramètres, créé par la fonction 'param_api_grit'
    :id_table : l'id de la table telle qu'indiquée à la page "Données sources"
    :data_update : un dataframe contenant les données à ajouter/modifier et la donnée de jointure
    :col_require : le nom de la colonne de jointure dans le dataframe 'data_update'
    '''
    #génère la chaine de caractère "str_json" qui correspond au json envoyé à l'api
    str_json = "{'records': ["
    for i in range(0, len(data_update)):
      dc = data_update.loc[i].to_dict()
      str_json = str_json + "{'require': " + str({k: v for k, v in dc.items() if k == col_require}) 
      str_json = str_json +  ", 'fields': " + str({k: v for k, v in dc.items() if k != col_require})
      str_json = str_json + "},"
    str_json = str_json[:-1] + "]}"
    str_json = str_json.replace('\'', '"')
    
    #contruction de l'url pour accéder aux données
    url_update = "%s/api/docs/%s/tables/%s/records" % (param_api["server"], param_api["id_doc"], id_table)
    
    #construction du header de l'appel de l'API, avec la clé utilisateur 'api_key' et type json
    headers = {
        'accept': '*/*',
        'Authorization': 'Bearer %s' % param_api["api_key"],
        # Already added when you pass json= but not when you pass data=
        # 'Content-Type': 'application/json',
      }
    #appel de l'API avec la transformation de str_json (type str) en json 
    return(requests.put(url_update, headers=headers, json=json.loads(str_json), proxies=param_api["proxies"]))
    
#fonction qui supprime des lignes d'une table en utilisant les numéros de ligne
def delete_row(param_api:dict, id_table:str, data_delete:list):
    ''' 
    Supprime une ou plusieurs ligne d'une table par son nom
    :param_api : un dictionnaire avec les paramètres, créé par la fonction 'param_api_grit'
    :id_table : l'id de la table telle qu'indiquée à la page "Données sources"
    :data_delete : une liste contenant les numéros des lignes à supprimer
     exemple : data_delete=[1,2,3] pour supprimer les lignes 1, 2 et 3
    '''
    #contruction de l'url pour accéder aux données
    url_delete = "%s/api/docs/%s/tables/%s/data/delete" % (param_api["server"], param_api["id_doc"], id_table)
    
    #construction du header de l'appel de l'API, avec la clé utilisateur 'api_key' et type json
    headers = {
        'accept': '*/*',
        'Authorization': 'Bearer %s' % param_api["api_key"],
        # Already added when you pass json= but not when you pass data=
        # 'Content-Type': 'application/json',
      }
    
    #appel de l'API
    return(requests.post(url_delete, headers=headers, json=data_delete, proxies=param_api["proxies"])) 